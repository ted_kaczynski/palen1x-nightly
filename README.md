# palen1x-nightly
<br>
basically [palen1x](https://github.com/palera1n/palen1x) but with palera1n nightly builds
<br>
do not ask for support with these, you must compile them yourself. 
<br>
none of palera1n team, nor me (genesis) are liable for any damages you may have caused to your device per the license in this project. 
<br>
anyways this will always grab the latest non-release palera1n build. 
<br>
to compile, just clone the project and run sudo ./build.sh. 
<br>
